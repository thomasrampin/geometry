package ex1;

public class Circle {

	private double rayon;
	private Point centre;
	
	public Circle(double rayon, Point centre){
		this.centre = centre;
		this.rayon = rayon;
	}

	public double getRayon() {
		return rayon;
	}

	public void setRayon(double rayon) {
		this.rayon = rayon;
	}

	public Point getCentre() {
		return centre;
	}

	public void setCentre(Point centre) {
		this.centre = centre;
	}
	
	public Point pointDiametraleOpp(Point p){
		return new Point(2*this.getCentre().getX() - p.getX(), 2*this.getCentre().getY() - p.getY());
	}
}
