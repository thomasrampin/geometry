package ex1;

public class Segment {

	protected Point a, b;
	protected double coefA, coefB;
	
	public Segment(Point a, Point b){
		this.a = a;
		this.b = b;
		this.coefA = ( a.getY() - b.getY() )/(a.getX() - b.getX());
		this.coefB = a.getY() - coefA*a.getX();
	}

	public Point getA() {
		return a;
	}

	public void setA(Point a) {
		this.a = a;
	}

	public Point getB() {
		return b;
	}

	public void setB(Point b) {
		this.b = b;
	}
	
	public double getCoefA() {
		return coefA;
	}

	public void setCoefA(double coefA) {
		this.coefA = coefA;
	}

	public double getCoefB() {
		return coefB;
	}

	public void setCoefB(double coefB) {
		this.coefB = coefB;
	}
	
	public Point milieu(){
		Point p;
		double x, y;
		x = (this.a.getX() + this.b.getX()) / 2;
		y = (this.a.getY() + this.b.getY()) / 2;
		
		p = new Point(x, y);
		return p;
	}

	public double length(){
		return this.a.distance(b);
	}
	
	public int isIntersection(Segment s){
		if(this.coefA == s.getCoefA()){
			return -1;
		}
		return 0;
	}
	
	public Point intersec(Segment s){
		double x, y;
		x = (s.getCoefB() - this.getCoefB())/(this.getCoefA() - s.getCoefA());
		y = s.getCoefA()*x + s.getCoefB();
		Point p = new Point(x, y);
		return p;
		
	}
	
	public boolean pBelong(Point p){
		return coefA*p.getX()+coefB >= p.getY()-5 && coefA*p.getX()+coefB <= p.getY()+5;
	}
}
