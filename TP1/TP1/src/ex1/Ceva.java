package ex1;

public class Ceva {

	private Triangle tABC;
	private Point p;
	private Point pAP,pBP,pCP;
	
	
	public Ceva(Triangle ABC, Point p) {
		
		ABC = ABC;
		p = p;
	}
	
	public Ceva(Triangle ABC) {
		ABC = ABC;
	}
	
	public Triangle getABC() {
		return tABC;
	}
	public void setABC(Triangle aBC) {
		tABC = aBC;
	}
	public Point getP() {
		return p;
	}
	public void setP(Point p) {
		p = p;
	}
	public Point getAP() {
		return pAP;
	}
	
	public void setAP() {
		Point A = tABC.getsAB().getA();
		Point vec = new Point(p.getX()-A.getX(),p.getY()-A.getY());
		Point far = new Point(p.getX()+vec.getX(),p.getY()+vec.getY());
		Segment Pf = new Segment(p,far);

		pAP = Pf.intersec(tABC.getsBC());
	}
	
	public void setOne(Point p){
		if(tABC.getsBC().pBelong(p)){
			pAP = new Point(p);
		}else if(tABC.getsAC().pBelong(p)){
			pBP = new Point(p);
		}else if(tABC.getsAB().pBelong(p)){
			pCP = new Point(p);
		}
	}
	
	public Point getBP() {
		return pBP;
	}
	public void setBP() {
		Point B = tABC.getsAB().getB();
		Point vec = new Point(p.getX()-B.getX(),p.getY()-B.getY());
		Point far = new Point(p.getX()+vec.getX(),p.getY()+vec.getY());
		Segment Pf = new Segment(p,far);
		pBP = Pf.intersec(tABC.getsAC());
	}
	public Point getCP() {
		return pCP;
	}
	public void setCP() {
		Point C = tABC.getsAC().getA();
		Point vec = new Point(p.getX()-C.getX(),p.getY()-C.getY());
		Point far = new Point(p.getX()+vec.getX(),p.getY()+vec.getY());
		Segment Pf = new Segment(p,far);
		pCP = Pf.intersec(tABC.getsAB());
	}
	
	
	public String getRapport(){
		Segment BBP,BPB,CCP,CPC,AAP,APA;
		BBP = new Segment(tABC.getsAB().getB(),pBP);
		BPB = new Segment(pBP,tABC.getsAB().getB());
		CCP = new Segment(tABC.getsAC().getA(),pCP);
		CPC = new Segment(pCP,tABC.getsAC().getA());
		AAP = new Segment(tABC.getsAB().getA(),pAP);
		APA = new Segment(pAP,tABC.getsAB().getA());
		double r = (BBP.length()/BPB.length())*(CCP.length()/CPC.length())*(AAP.length()/APA.length());
		return new String(String.valueOf(r));
	}

	public void setP() {
		if(pAP!=null && pBP!=null){
			Segment AAP = new Segment(tABC.getsAB().getA(),pAP);
			Segment BBP = new Segment(tABC.getsAB().getB(),pBP);
			p = AAP.intersec(BBP);
		}else if(pAP!=null && pCP!=null){
			Segment AAP = new Segment(tABC.getsAB().getA(),pAP);
			Segment CCP = new Segment(tABC.getsAC().getA(),pCP);
			p = AAP.intersec(CCP);
		}else if(pCP!=null && pBP!=null){
			Segment CCP = new Segment(tABC.getsAC().getA(),pCP);
			Segment BBP = new Segment(tABC.getsAB().getB(),pBP);
			p = CCP.intersec(BBP);
		}
		
	}
}
