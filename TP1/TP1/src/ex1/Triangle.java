package ex1;

public class Triangle {
	
	protected Segment sAB, sAC, sBC;
	protected Point pA, pB, pC;
	
	public Triangle(Point pA, Point pB, Point pC) {
		this.sAB = new Segment(pA, pB);
		this.sAC = new Segment(pA, pC);
		this.sBC = new Segment(pB, pC);
	}

	public Segment getsAB() {
		return sAB;
	}

	public void setsAB(Segment sAB) {
		this.sAB = sAB;
	}

	public Segment getsAC() {
		return sAC;
	}

	public void setsAC(Segment sAC) {
		this.sAC = sAC;
	}

	public Segment getsBC() {
		return sBC;
	}

	public void setsBC(Segment sBC) {
		this.sBC = sBC;
	}

	public Point getA(){
		return this.getsAB().getA();
	}
	
	public Point getB(){
		return this.getsAB().getB();
	}
	
	public Point getC(){
		return this.getsAC().getB();
	}
	
	public Segment getLittleA(){
		return this.getsBC();
	}
	
	public Segment getLittleB(){
		return this.getsAC();
	}
	
	public Segment getLittleC(){
		return this.getsAB();
	}
	
	public Point centerCir(){
		Point p;
		double x, y, d;
		d = 2*((this.getA().getX()*(this.getB().getY() - this.getC().getY()) + (this.getB().getX())*(this.getC().getY() - this.getA().getY()) + (this.getC().getX() * (this.getA().getY() - this.getB().getY()))));
		x = 1/d * (((Math.pow(this.getA().getX(),2) + Math.pow(this.getA().getY(),2))*(this.getB().getY() - this.getC().getY()))+((Math.pow(this.getB().getX(),2) + Math.pow(this.getB().getY(), 2))*(this.getC().getY() - this.getA().getY()))+((Math.pow(this.getC().getX(), 2) + Math.pow(this.getC().getY(), 2))*(this.getA().getY() - this.getB().getY())));
		y = 1/d * (((Math.pow(this.getA().getX(), 2) + Math.pow(this.getA().getY(), 2))*(this.getC().getX() - this.getB().getX()))+((Math.pow(this.getB().getX(), 2) + Math.pow(this.getB().getY(), 2))*(this.getA().getX() - this.getC().getX()))+((Math.pow(this.getC().getX(), 2) + Math.pow(this.getC().getY(), 2))*(this.getB().getX() - this.getA().getX())));
		p = new Point(x, y);
		return p;
	}
	
	public double rayonCC(){
			double r=0;
			Point vecA = new Point(this.getB().getX()-this.getA().getX(),this.getB().getY()-this.getA().getY());
			Point vecB = new Point(this.getC().getX()-this.getA().getX(),this.getC().getY()-this.getA().getY());
			
			/*get angle*/
			double dot = vecA.getX()*vecB.getX()+vecA.getY()*vecB.getY();
			double length_vecA = Math.sqrt(Math.pow(this.getB().getX()-this.getA().getX(), 2)+Math.pow(this.getB().getY()-this.getA().getY(), 2));
			double length_vecB = Math.sqrt(Math.pow(this.getC().getX()-this.getA().getX(), 2)+Math.pow(this.getC().getY()-this.getA().getY(), 2));
			double angle = Math.acos(dot/(length_vecA*length_vecB));
			
			r = this.getsBC().length()/(Math.sin(angle));
			
			return r;
	}
}
